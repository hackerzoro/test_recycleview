package com.example.myapplication;


import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Layout;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    TextView txt;
    RecyclerView rcl_list_Code;
    List<String> list;
    View view;
    RecyclerView.Adapter adapter;
    RecyclerView.LayoutManager layoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initControl();


        layoutManager = new LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL,false);
        rcl_list_Code.setLayoutManager(layoutManager);

        adapter = new MyAdapter(list);
        rcl_list_Code.setAdapter(adapter);

        txt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                int pos = txt.getSelectionStart();
//                Layout layout = txt.getLayout();
//                int line = layout.getLineForOffset(pos);
//                int baseline = layout.getLineBaseline(line);
//                int ascent = layout.getLineAscent(line);
//                float x = layout.getPrimaryHorizontal(pos);
//                float y = baseline + ascent;
//                rcl_list_Code.setX(x);
//                rcl_list_Code.setY(y);
                rcl_list_Code.setVisibility(View.VISIBLE);
            }
        });
        rcl_list_Code.addOnItemTouchListener(new RecyclerViewTouchListener(getApplicationContext(), rcl_list_Code, new OnItemClickListener() {
            @Override
            public void onItemClick(View view, int pos) {
                txt.setText(list.get(pos));
                rcl_list_Code.setVisibility(View.GONE);
            }

        }));

    }

    private void initControl() {
        txt = findViewById(R.id.txt);
        rcl_list_Code = findViewById(R.id.rcl_list_Code);
        list = new ArrayList<>();
        list.add("Đừng");
        list.add("Yêu");
        list.add("Nữa");
        list.add("Em ");
        list.add("Mệt");
        list.add("Rồi");
        list.add("Đừng");
        list.add("Yêu");
        list.add("Nữa");
        list.add("Đừng");
        list.add("Yêu");
        list.add("Nữa");
        list.add("Em ");
        list.add("Đừng");
        list.add("Yêu");
        list.add("Nữa");
        list.add("Em ");
        list.add("Mệt");
        list.add("Rồi");
        list.add("Yêu");
        list.add("Nữa");
        list.add("Em ");
        list.add("Mệt");
    }
}
